set :stage, :production
set :branch, :production
set :user, "www-data"
set :tmp_dir, "/tmp"

# Change the :deploy_to variable for setting the deployment path
# ==============================================================
set :deploy_to, "/httpdocs/wordpress/"
set :shared_path, "#{deploy_to}/shared"
SSHKit.config.command_map[:composer] = "php #{shared_path}/composer.phar"

# Simple Role Syntax
# ==================
role :app, %w{www-data@tiberius.sui-inter.net:2121}
role :web, %w{www-data@tiberius.sui-inter.net:2121}
role :db,  %w{www-data@tiberius.sui-inter.net:2121}

# Extended Server Syntax
# ======================
server 'tiberius.sui-inter.net:2121', user: 'www-data', roles: %w{web app db}

fetch(:default_env).merge!(wp_env: :production)

