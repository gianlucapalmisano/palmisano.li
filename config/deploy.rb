set :application, 'palmisano.li'
set :repo_url, 'https://gianlucapalmisano@bitbucket.org/gianlucapalmisano/palmisano.li.git'

# Branch options
# Prompts for the branch name (defaults to current branch)
ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

set :deploy_to, "/home/httpd/vhosts/#{fetch(:application)}"
set :shared_path, "#{deploy_to}/shared"

# Use :debug for more verbose output when troubleshooting
set :log_level, :info

# Apache users with .htaccess files:
# it needs to be added to linked_files so it persists across deploys:
set :linked_files, fetch(:linked_files, []).push('.env', 'web/.htaccess')
set :linked_dirs, fetch(:linked_dirs, []).push('web/app/uploads')

# Install Composer executable
SSHKit.config.command_map[:composer] = "php #{shared_path}/composer.phar"
namespace :deploy do
  before :starting, 'composer:install_executable'
end

# # The Roots theme by default does not check production assets into Git, so
# # they are not deployed by Capistrano when using the Bedrock stack. The
# # following will compile and deploy those assets. Copy this to the bottom of
# # your config/deploy.rb file.
 
# # Based on information from this thread:
# # http://discourse.roots.io/t/capistrano-run-grunt-locally-and-upload-files/2062/7
# # and specifically this gist from christhesoul:
# # https://gist.github.com/christhesoul/3c38053971a7b786eff2
 
# # First we need to set some variables so we know where things are. You should
# # only have to modify :theme_path here, :local_app_path and :local_theme_path
# # are set from that.
# set :theme_path, Pathname.new('web/app/themes/uncode-child')
# set :local_app_path, Pathname.new(File.dirname(__FILE__)).join('../')
# set :local_theme_path, fetch(:local_app_path).join(fetch(:theme_path))
 
# # Next we list the production assets we want to deploy. We could change things
# # around so that all our production assets are generated into a single
# # directory and upload that, but it would involve touching a lot of things.
# # Listing them each explicitly keeps our changes to just the deployment
# # configuration.
# set :production_assets, [
#   'dist/styles/main.css',
#   'dist/scripts/main.js',
#   'dist/scripts/modernizr.js',
#   'dist/scripts/jquery.js'].map {|path| Pathname.new(path) }
 
# namespace :deploy do

#   # The :compile_assets task will run 'grunt build' in our theme directory
#   # to build the production assets.
#   task :compile_assets do
#     run_locally do
#       execute "cd '#{fetch(:local_theme_path)}'; npm install --production --silent --no-spin;"
#     end
#   end
 
#   # The :copy_assets task first runs :compile_assets, then goes through the
#   # list of production assets and uploads them to the server. It also creates
#   # the target directories if necessary.
#   task :copy_assets do
#     invoke 'deploy:compile_assets'
#     on roles(:web) do
#       fetch(:production_assets).each do |path|
#         execute :mkdir, "-p", release_path.join(fetch(:theme_path)).join(path.dirname())
#         upload! fetch(:local_theme_path).join(path).to_s, release_path.join(fetch(:theme_path)).join(path)
#       end
#     end
#   end

# end
 
# # This tells Capistrano to copy our production assets to the server after it
# # has created the release directory but before it has published the release.
# before "deploy:updated", "deploy:copy_assets"

# namespace :deploy do
#   desc 'Update WordPress template root paths to point to the new release'
#   task :update_option_paths do
#     on roles(:app) do
#       within fetch(:release_path) do
#         if test :wp, :core, 'is-installed'
#           [:stylesheet_root, :template_root].each do |option|
#             # Only change the value if it's an absolute path
#             # i.e. The relative path "/themes" must remain unchanged
#             # Also, the option might not be set, in which case we leave it like that
#             value = capture :wp, :option, :get, option, raise_on_non_zero_exit: false
#             if value != '' && value != '/themes'
#               execute :wp, :option, :set, option, fetch(:release_path).join('web/wp/wp-content/themes')
#             end
#           end
#         end
#       end
#     end
#   end
# end

# The above update_option_paths task is not run by default
# Note that you need to have WP-CLI installed on your server
# Uncomment the following line to run it on deploys if needed
# after 'deploy:publishing', 'deploy:update_option_paths'