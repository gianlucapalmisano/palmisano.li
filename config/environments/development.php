<?php
/** Development */
define('SAVEQUERIES', true);
define('WP_DEBUG', false);
define('SCRIPT_DEBUG', false);
define('DISALLOW_FILE_MODS', false);
define('FS_METHOD', 'direct');