<?php

/** @var string Directory containing all of the site's files */
$root_dir = dirname(__DIR__);

/** @var string Document Root */
$webroot_dir = $root_dir . '/web';

/**
 * Expose global env() function from oscarotero/env
 */
Env::init();

/**
 * Use Dotenv to set required environment variables and load .env file in root
 */
$dotenv = new Dotenv\Dotenv($root_dir);
if (file_exists($root_dir . '/.env')) {
    $dotenv->load();
    $dotenv->required(['DB_NAME', 'DB_USER', 'DB_PASSWORD', 'WP_HOME', 'WP_SITEURL']);
}

/**
 * Set up our global environment constant and load its config first
 * Default: development
 */
define('WP_ENV', env('WP_ENV') ?: 'development');

$env_config = __DIR__ . '/environments/' . WP_ENV . '.php';

if (file_exists($env_config)) {
    require_once $env_config;
}

/**
 * URLs
 */
define('WP_HOME', env('WP_HOME'));
define('WP_SITEURL', env('WP_SITEURL'));

/**
 * Custom Content Directory
 */
define('CONTENT_DIR', '/app');
define('WP_CONTENT_DIR', $webroot_dir . CONTENT_DIR);
define('WP_CONTENT_URL', WP_HOME . CONTENT_DIR);

/**
 * DB settings
 */
define('DB_NAME', env('DB_NAME'));
define('DB_USER', env('DB_USER'));
define('DB_PASSWORD', env('DB_PASSWORD'));
define('DB_HOST', env('DB_HOST') ?: 'localhost');
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');
$table_prefix = env('DB_PREFIX') ?: 'wp_';

/**
 * Authentication Unique Keys and Salts
 */
define('AUTH_KEY', env('AUTH_KEY'));
define('SECURE_AUTH_KEY', env('SECURE_AUTH_KEY'));
define('LOGGED_IN_KEY', env('LOGGED_IN_KEY'));
define('NONCE_KEY', env('NONCE_KEY'));
define('AUTH_SALT', env('AUTH_SALT'));
define('SECURE_AUTH_SALT', env('SECURE_AUTH_SALT'));
define('LOGGED_IN_SALT', env('LOGGED_IN_SALT'));
define('NONCE_SALT', env('NONCE_SALT'));

/**
 * Custom Settings
 */
define('AUTOMATIC_UPDATER_DISABLED', true);
define('DISABLE_WP_CRON', env('DISABLE_WP_CRON') ?: false);
define('DISALLOW_FILE_EDIT', true);

/**
 * Bootstrap WordPress
 */
if (!defined('ABSPATH')) {
    define('ABSPATH', $webroot_dir . '/wp/');
}


/**
 * WP-CLI-DEPLOY Settings
 */
define( 'DEVELOPMENT_URL', getenv('DEVELOPMENT_URL'));
define( 'DEVELOPMENT_WP_PATH', getenv('DEVELOPMENT_WP_PATH'));
define( 'DEVELOPMENT_HOST', getenv('DEVELOPMENT_HOST'));
define( 'DEVELOPMENT_USER', getenv('DEVELOPMENT_USER'));
define( 'DEVELOPMENT_PORT', '22' );
define( 'DEVELOPMENT_PATH', getenv('DEVELOPMENT_PATH'));
define( 'DEVELOPMENT_UPLOADS_PATH', getenv('DEVELOPMENT_UPLOADS_PATH') );
define( 'DEVELOPMENT_THEMES_PATH', getenv('DEVELOPMENT_THEMES_PATH') );
define( 'DEVELOPMENT_PLUGINS_PATH', getenv('DEVELOPMENT_PLUGINS_PATH') );
define( 'DEVELOPMENT_DB_HOST', getenv('DEVELOPMENT_DB_HOST'));
define( 'DEVELOPMENT_DB_NAME', getenv('DEVELOPMENT_DB_NAME'));
define( 'DEVELOPMENT_DB_USER', getenv('DEVELOPMENT_DB_USER'));
define( 'DEVELOPMENT_DB_PASSWORD', getenv('DEVELOPMENT_DB_PASSWORD'));

define( 'STAGING_URL', getenv('STAGING_URL'));
define( 'STAGING_WP_PATH', getenv('STAGING_WP_PATH'));
define( 'STAGING_HOST', getenv('STAGING_HOST'));
define( 'STAGING_USER', getenv('STAGING_USER'));
define( 'STAGING_PORT', '22' );
define( 'STAGING_PATH', getenv('STAGING_PATH'));
define( 'STAGING_UPLOADS_PATH', getenv('STAGING_UPLOADS_PATH') );
define( 'STAGING_THEMES_PATH', getenv('STAGING_THEMES_PATH') );
define( 'STAGING_PLUGINS_PATH', getenv('STAGING_PLUGINS_PATH') );
define( 'STAGING_DB_HOST', getenv('STAGING_DB_HOST'));
define( 'STAGING_DB_NAME', getenv('STAGING_DB_NAME'));
define( 'STAGING_DB_USER', getenv('STAGING_DB_USER'));
define( 'STAGING_DB_PASSWORD', getenv('STAGING_DB_PASSWORD'));

define( 'DEV_URL', getenv('DEV_URL'));
define( 'DEV_WP_PATH', getenv('DEV_WP_PATH'));
define( 'DEV_HOST', getenv('DEV_HOST'));
define( 'DEV_USER', getenv('DEV_USER'));
define( 'DEV_PORT', '22' );
define( 'DEV_PATH', getenv('DEV_PATH'));
define( 'DEV_UPLOADS_PATH', getenv('DEV_UPLOADS_PATH') );
define( 'DEV_THEMES_PATH', getenv('DEV_THEMES_PATH') );
define( 'DEV_PLUGINS_PATH', getenv('DEV_PLUGINS_PATH') );
define( 'DEV_DB_HOST', getenv('DEV_DB_HOST'));
define( 'DEV_DB_NAME', getenv('DEV_DB_NAME'));
define( 'DEV_DB_USER', getenv('DEV_DB_USER'));
define( 'DEV_DB_PASSWORD', getenv('DEV_DB_PASSWORD'));

define( 'PRODUCTION_URL', getenv('PRODUCTION_URL'));
define( 'PRODUCTION_WP_PATH', getenv('PRODUCTION_WP_PATH'));
define( 'PRODUCTION_HOST', getenv('PRODUCTION_HOST'));
define( 'PRODUCTION_USER', getenv('PRODUCTION_USER'));
define( 'PRODUCTION_PORT', '22' );
define( 'PRODUCTION_PATH', getenv('PRODUCTION_PATH'));
define( 'PRODUCTION_UPLOADS_PATH', getenv('PRODUCTION_UPLOADS_PATH') );
define( 'PRODUCTION_THEMES_PATH', getenv('PRODUCTION_THEMES_PATH') );
define( 'PRODUCTION_PLUGINS_PATH', getenv('PRODUCTION_PLUGINS_PATH') );
define( 'PRODUCTION_DB_HOST', getenv('PRODUCTION_DB_HOST'));
define( 'PRODUCTION_DB_NAME', getenv('PRODUCTION_DB_NAME'));
define( 'PRODUCTION_DB_USER', getenv('PRODUCTION_DB_USER'));
define( 'PRODUCTION_DB_PASSWORD', getenv('PRODUCTION_DB_PASSWORD'));