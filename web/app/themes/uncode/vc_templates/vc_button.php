<?php
$url = $link = $target = $button_color = $size = $width = $outline = $wide = $icon = $icon_position = $icon_animation = $border_animation = $radius = $shadow = $italic = $display = $top_margin = $onclick = $rel = $media_lightbox = $css_animation = $animation_delay = $animation_speed = $el_class = '';
extract(shortcode_atts(array(
	'url' => '',
	'link' => '',
	'target' => 'self',
	'button_color' => 'default',
	'size' => '',
	'width' => '',
	'outline' => '',
	'wide' => 'no',
	'icon' => '',
	'icon_position' => 'left',
	'icon_animation' => '',
	'border_animation' => '',
	'radius' => '',
	'shadow' => '',
	'italic' => '',
	'display' => '',
	'top_margin' => '',
	'onclick' => '',
	'rel' => '',
	'media_lightbox' => '',
	'css_animation' => '',
	'animation_delay' => '',
	'animation_speed' => '',
	'el_class' => ''
) , $atts));

//parse link
$link = ( $link == '||' ) ? '' : $link;
$link = vc_build_link( $link );
$a_href = $link['url'];
$a_title = $link['title'];
$a_target = $link['target'];

if ($media_lightbox !== '') {
	$media_attributes = uncode_get_media_info($media_lightbox);
	if (isset($media_attributes)) {
		$media_metavalues = unserialize($media_attributes->metadata);
		$media_mime = $media_attributes->post_mime_type;
		if (strpos($media_mime, 'image/') !== false && $media_mime !== 'image/url' && isset($media_metavalues['width']) && isset($media_metavalues['height'])) {
			$image_orig_w = $media_metavalues['width'];
			$image_orig_h = $media_metavalues['height'];
			$big_image = uncode_resize_image($media_attributes->guid, $media_attributes->path, $image_orig_w, $image_orig_h, 12, null, false);
			$a_href = $big_image['url'];
		} else {
			if ($media_mime === 'image/url') {
				$a_href = $media_attributes->guid;
			} else {
				$media_oembed = uncode_get_oembed($media_lightbox, $media_attributes->guid, $media_attributes->post_mime_type, false, $media_attributes->post_excerpt, $media_attributes->post_content, true);
				if ($media_mime === 'oembed/html' || $media_mime === 'oembed/iframe') {
					$frame_id = 'frame-' . big_rand();
					$a_href = '#' . $frame_id;
					echo '<div id="'.$frame_id.'" style="display: none;">' . $media_attributes->post_content . '</div>';
				} else $a_href = $media_oembed['code'];
			}
		}
	}
	$lightbox_data = ' data-lbox="ilightbox_single-'.big_rand().'"';
} else $lightbox_data = '';

// Prepare button classes
$wrapper_class = array('btn-container');
$classes = array('btn');
$div_data = array();

// Additional classes
if ($el_class) $classes[] = $el_class;

// Size class
if ($size) $classes[] = $size;

// Color class
if ($button_color === '') $button_color = 'default';
if ($size !== 'btn-link') $classes[] = 'btn-' . $button_color;
else $classes[] = 'text-' . $button_color . '-color';

// Radius class
if ($radius) $classes[] = $radius;

// Outlined class
if ($outline === 'yes') $classes[] = 'btn-outline';

// Shadow class
if ($shadow === 'yes') $classes[] = 'btn-shadow';

// Italic class
if ($italic === 'yes') $classes[] = 'btn-italic';

// Wide class
if ($wide === 'yes') {
	$wrapper_class[] = 'btn-block';
	$classes[] = 'btn-block';
}

if ($display === 'inline') {
	// Add margin class
	if ($top_margin === 'yes') $classes[] = 'btn-top-margin';
	$wrapper_class[] = 'btn-inline';
}


// Prepare icon
if ($icon !== '') {
	$icon = '<i class="' . esc_attr($icon) . '"></i>';
	if ($icon_animation === 'yes') $classes[] = 'btn-icon-fx';
}
else $icon = '';

$content = trim($content);

if ($icon_position === 'right')
{
	$content = $content . $icon;
	$classes[] = 'btn-icon-right';
} else {
	$content = $icon . $content;
	$classes[] = 'btn-icon-left';
}

if ($border_animation !== '') {
	$classes[] = $border_animation;
	$classes[] = 'btn-border-animated';
}
$el_class = $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, ' ' . implode($classes, ' ') . $el_class, $this->settings['base'], $atts );

// Prepare onclick action
$onclick = ($onclick !== '') ? ' onClick="' . esc_js($onclick) . '"' : '';

// Prepare rel attribute
$rel = ($rel) ? ' rel="' . esc_attr($rel) . '"' : '';

if ($css_animation !== '') {
	$wrapper_class[] = 'animate_when_almost_visible ' . $css_animation;
	if ($animation_delay !== '') $div_data['data-delay'] = $animation_delay;
	if ($animation_speed !== '') $div_data['data-speed'] = $animation_speed;
}

if ($width !== '') $width = ' style="min-width:' . $width . 'px"';

$title = ($a_title !== '') ? ' title="' . $a_title . '"' : '';
$target = (trim($a_target) !== '') ? ' target="' . trim($a_target) . '"' : '';

echo '<span class="' . esc_attr(trim(implode($wrapper_class, ' '))) . '" '.implode(' ', array_map(function ($v, $k) { return $k . '="' . $v . '"'; }, $div_data, array_keys($div_data))).'><a href="' . esc_url($a_href) . '" class="' . esc_attr(trim(implode($classes, ' '))) . '"' . $title . $target . $onclick . $rel . $lightbox_data . $width . '>' . do_shortcode($content) . '</a></span>';
