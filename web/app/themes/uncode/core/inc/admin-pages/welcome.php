<?php
/**
 * Admin View: Page - Status Report
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function uncode_let_to_num( $size ) {
  $l   = substr( $size, -1 );
  $ret = substr( $size, 0, -1 );
  switch ( strtoupper( $l ) ) {
    case 'P':
      $ret *= 1024;
    case 'T':
      $ret *= 1024;
    case 'G':
      $ret *= 1024;
    case 'M':
      $ret *= 1024;
    case 'K':
      $ret *= 1024;
  }
  return $ret;
}

?>

<div class="wrap about-wrap uncode-wrap">

	<h1><?php echo esc_html__( "Welcome to ", "uncode" ) . '<span class="uncode-name">'.UNCODE_NAME.'</span>'; ?><span class="uncode-version"><?php echo UNCODE_VERSION; ?></span></h1>

	<div class="about-text">
		<?php printf(wp_kses(__( "%s is up and running! Check that all the requirements below are fulfilled and labeled in green.<br>Enjoy and free your imagination with %s!", "uncode" ), array( 'br' => '')), UNCODE_NAME, UNCODE_NAME); ?>
	</div>
	<h2 class="nav-tab-wrapper">
    <?php
			printf( '<a href="%s" class="nav-tab nav-tab-active">%s</a>', admin_url('admin.php?page=uncode-options'), esc_html__( "System Status", "uncode" ) );
			printf( '<a href="%s" class="nav-tab">%s</a>', admin_url( 'admin.php?page=one-click-demo' ), esc_html__( "Install Demo", "uncode" ) );
			printf( '<a href="%s" class="nav-tab">%s</a>', admin_url('admin.php?page=uncode-options'), esc_html__( "Theme Options", "uncode" ) );
			printf( '<a href="%s" class="nav-tab">%s</a>', admin_url( 'admin.php?page=ot-settings' ), esc_html__( "Option Tools", "uncode" ) );
			printf( '<a href="%s" class="nav-tab">%s</a>', admin_url( 'admin.php?page=uncodefont' ), esc_html__( "Install Fonts", "uncode" ) );
      printf( '<a href="%s" class="nav-tab">%s</a>', admin_url( 'admin.php?page=uncodefont-settings' ), esc_html__( "Font Sources", "uncode" ) );
		?>
	</h2>
	<table class="widefat" cellspacing="0" id="status">
		<thead>
			<tr>
				<th colspan="3" data-export-label="WordPress Environment"><?php esc_html_e( 'WordPress Environment', 'uncode' ); ?></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td data-export-label="Home URL" style="width: 33%;"><?php esc_html_e( 'Home URL', 'uncode' ); ?>:</td>
				<td class="help" style="width: 15px;"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'The URL of your site\'s homepage.', 'uncode' ) . '">[?]</a>'; ?></td>
				<td><?php echo esc_url( home_url( '/' ) ); ?></td>
			</tr>
			<tr>
				<td data-export-label="Site URL"><?php esc_html_e( 'Site URL', 'uncode' ); ?>:</td>
				<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'The root URL of your site.', 'uncode' ) . '">[?]</a>'; ?></td>
				<td><?php echo site_url(); ?></td>
			</tr>
			<tr>
				<td data-export-label="Frontend stylesheet"><?php esc_html_e( 'Frontend stylesheet', 'uncode' ); ?>:</td>
				<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'Uncode is generating a stylesheet when the options are saved. The file must be writtable.', 'uncode' ) . '">[?]</a>'; ?></td>
				<td><?php
					global $wp_filesystem;
					if (empty($wp_filesystem)) {
					    require_once (ABSPATH . '/wp-admin/includes/file.php');
					    WP_Filesystem();
					}
					$access_type = get_filesystem_method();
					$front_css = get_template_directory() . '/library/css/style-custom.css';
					if ($access_type === 'direct') {
						if ( @fopen( $front_css, 'a' ) ) {
							echo '<mark class="yes">' . '&#10004; <code>' . $front_css .'</code></mark> ';
						} else {
							printf( '<mark class="error">' . '<i class="fa fa-cross"></i> - ' . wp_kses(__( 'To allow the correct style to work, make <code>%s</code> writable.', 'uncode' ), array( 'code' => '' )) . '</mark>', $front_css );
						}
					} else {
						printf( '<div style="color:#0073aa;">' . '&#10004; - ' . wp_kses(__( 'WordPress doesn\'t have direct access to this file <code>%s</code> due to a confict in the Uncode folder permission or your configuration of WordPress file access is not the direct method. The custom css will be output inline.', 'uncode' ), array( 'code' => '' )) . '</div>', $front_css  );
					}
				?></td>
			</tr>
			<tr>
				<td data-export-label="Font stylesheet"><?php esc_html_e( 'Backend stylesheet', 'uncode' ); ?>:</td>
				<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'Uncode is generating a stylesheet when the options are saved. The file must be writtable.', 'uncode' ) . '">[?]</a>'; ?></td>
				<td><?php
				$back_css = get_template_directory() . '/core/assets/css/admin-custom.css';
					if ($access_type === 'direct') {
						if ( @fopen( $back_css, 'a' ) ) {
							echo '<mark class="yes">' . '&#10004; <code>' . $back_css .'</code></mark> ';
						} else {
							printf( '<mark class="error">' . '<i class="fa fa-cross"></i> - ' . wp_kses(__( 'To allow the correct style to work, make <code>%s</code> writable.', 'uncode' ), array( 'code' => '' )) . '</mark>', $back_css );
						}
					} else {
						printf( '<div style="color:#0073aa;">' . '&#10004; - ' . wp_kses(__( 'WordPress doesn\'t have direct access to this file <code>%s</code> due to a confict in the Uncode folder permission or your configuration of WordPress file access is not the direct method. The custom css will be output inline.', 'uncode' ), array( 'code' => '' )) . '</div>', $back_css  );
					}
				?></td>
			</tr>
			<tr>
				<td data-export-label="WP Version"><?php esc_html_e( 'WP Version', 'uncode' ); ?>:</td>
				<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'The version of WordPress installed on your site.', 'uncode' ) . '">[?]</a>'; ?></td>
				<td><?php bloginfo('version'); ?></td>
			</tr>
			<tr>
				<td data-export-label="WP Multisite"><?php esc_html_e( 'WP Multisite', 'uncode' ); ?>:</td>
				<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'Whether or not you have WordPress Multisite enabled.', 'uncode' ) . '">[?]</a>'; ?></td>
				<td><?php if ( is_multisite() ) echo '&#10004;'; else echo '&ndash;'; ?></td>
			</tr>
			<tr>
				<td data-export-label="WP Memory Limit"><?php esc_html_e( 'WP Memory Limit', 'uncode' ); ?>:</td>
				<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'The maximum amount of memory (RAM) that your site can use at one time.', 'uncode' ) . '">[?]</a>'; ?></td>
				<td><?php
					$memory = uncode_let_to_num( WP_MEMORY_LIMIT );

					if ( $memory < 67108864 ) {
						echo '<mark class="error">' . sprintf( wp_kses(__( '%s - We recommend setting memory to at least 64MB. See: <a href="%s" target="_blank">Increasing memory allocated to PHP</a>', 'uncode' ), array( 'a' => array( 'href' => array(),'target' => array() ) ) ), size_format( $memory ), 'http://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP' ) . '</mark>';
					} else {
						echo '<mark class="yes">' . size_format( $memory ) . '</mark>';
					}
				?></td>
			</tr>
			<tr>
				<td data-export-label="WP Debug Mode"><?php esc_html_e( 'WP Debug Mode', 'uncode' ); ?>:</td>
				<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'Displays whether or not WordPress is in Debug Mode.', 'uncode' ) . '">[?]</a>'; ?></td>
				<td><?php if ( defined('WP_DEBUG') && WP_DEBUG ) echo '<mark class="yes">' . '&#10004;' . '</mark>'; else echo '&ndash;'; ?></td>
			</tr>
			<tr>
				<td data-export-label="Language"><?php esc_html_e( 'Language', 'uncode' ); ?>:</td>
				<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'The current language used by WordPress. Default = English', 'uncode' ) . '">[?]</a>'; ?></td>
				<td><?php echo get_locale() ?></td>
			</tr>
		</tbody>
	</table>
	<table class="widefat" cellspacing="0" id="status">
		<thead>
			<tr>
				<th colspan="3" data-export-label="Server Environment"><?php esc_html_e( 'Server Environment', 'uncode' ); ?></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td data-export-label="Server Info" style="width: 33%;"><?php esc_html_e( 'Server Info', 'uncode' ); ?>:</td>
				<td class="help" style="width: 15px;"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'Information about the web server that is currently hosting your site.', 'uncode' ) . '">[?]</a>'; ?></td>
				<td><?php echo esc_html( $_SERVER['SERVER_SOFTWARE'] ); ?></td>
			</tr>
			<tr>
				<td data-export-label="PHP Version"><?php esc_html_e( 'PHP Version', 'uncode' ); ?>:</td>
				<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'The version of PHP installed on your hosting server.', 'uncode' ) . '">[?]</a>'; ?></td>
				<td><?php
					// Check if phpversion function exists
					if ( function_exists( 'phpversion' ) ) {
						$php_version = phpversion();
						if (version_compare($php_version,'5.6.0') >= 0) {
							echo '<mark class="yes">' . esc_html( $php_version ) . '</mark>';
						} else if (version_compare($php_version,'5.3.0') >= 0) {
							echo '<div style="color:#0073aa;">' . esc_html( $php_version ) . ' - ' . esc_html__( 'The recommended version is the 5.6.','uncode' ) . '</div>';
						} else {
							echo '<mark class="error">' . esc_html( $php_version ) . ' - ' . esc_html__( 'You are running an obsolete version of PHP. For your security and a proper functioning of the theme you have to upgrade it a newer version (5.6 is recommended). Please contact your hosting provider.','uncode' ) . '</mark>';
						}
					} else {
						esc_html_e( "Couldn't determine PHP version because phpversion() doesn't exist.", 'uncode' );
					}
					?></td>
			</tr>
			<?php if ( function_exists( 'ini_get' ) ) : ?>
				<tr>
					<td data-export-label="PHP Post Max Size"><?php esc_html_e( 'PHP Post Max Size', 'uncode' ); ?>:</td>
					<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'The largest filesize that can be contained in one post.', 'uncode' ) . '">[?]</a>'; ?></td>
					<td><?php
					$php_max_size = (int) ini_get('post_max_size');
					if ( $php_max_size < 32 ) {
						echo '<mark class="error">' . sprintf( wp_kses(__( '%s - Raccomended value is at least 32. <a href="%s" target="_blank">Please increase it.</a>', 'uncode' ), array( 'a' => array( 'href' => array(),'target' => array() ) ) ), size_format( uncode_let_to_num( ini_get('post_max_size') ) ), 'https://www.a2hosting.com/kb/developer-corner/php/using-php-directives-in-custom-htaccess-files/setting-the-php-maximum-upload-file-size-in-an-htaccess-file' ) . '</mark>';
					} else {
						echo '<mark class="yes">' . size_format( uncode_let_to_num( ini_get('post_max_size') ) ) . '</mark>';
					}
					?></td>
				</tr>
				<tr>
					<td data-export-label="PHP Time Limit"><?php esc_html_e( 'PHP Time Limit', 'uncode' ); ?>:</td>
					<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'The amount of time (in seconds) that your site will spend on a single operation before timing out (to avoid server lockups)', 'uncode' ) . '">[?]</a>'; ?></td>
					<td><?php
					$php_max_time = ini_get('max_execution_time');
					if ( $php_max_time < 120 ) {
						echo '<mark class="error">' . sprintf( wp_kses(__( '%s - Raccomended value is at least 120. <a href="%s" target="_blank">Please increase it.</a>', 'uncode' ), array( 'a' => array( 'href' => array(),'target' => array() ) ) ), $php_max_time, 'http://codex.wordpress.org/Common_WordPress_Errors#Maximum_execution_time_exceeded' ) . '</mark>';
					} else {
						echo '<mark class="yes">' . $php_max_time . '</mark>';
					}
					?></td>
				</tr>
				<tr>
				<?php
					$max_input = ini_get('max_input_vars');
					if ( $max_input < 3000 ) {
						$max_input = '<span class="error">' . $max_input . '  -  ' . sprintf( wp_kses(__( 'Raccomended value is at least 3000. <a href="%s">Please increase it.</a>', 'uncode' ), array( 'a' => array( 'href' => array(),'target' => array() ) ) ), 'http://docs.woothemes.com/document/problems-with-large-amounts-of-data-not-saving-variations-rates-etc/#section-2' ) . '</span>';
					} else {
						$max_input = '<mark class="yes">' . $max_input . '</mark>';
					}
				?>
					<td data-export-label="PHP Max Input Vars"><?php esc_html_e( 'PHP Max Input Vars', 'uncode' ); ?>:</td>
					<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'The maximum number of variables your server can use for a single function to avoid overloads.', 'uncode' ) . '">[?]</a>'; ?></td>
					<td><?php echo wp_kses_post($max_input); ?></td>
				</tr>
				<tr>
					<td data-export-label="SUHOSIN Installed"><?php esc_html_e( 'SUHOSIN Installed', 'uncode' ); ?>:</td>
					<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'Suhosin is an advanced protection system for PHP installations. It was designed to protect your servers on the one hand against a number of well known problems in PHP applications and on the other hand against potential unknown vulnerabilities within these applications or the PHP core itself. If enabled on your server, Suhosin may need to be configured to increase its data submission limits.', 'uncode' ) . '">[?]</a>'; ?></td>
					<td><?php echo extension_loaded( 'suhosin' ) ? '&#10004;' : '&ndash;'; ?></td>
				</tr>
			<?php endif; ?>
			<tr>
				<td data-export-label="MySQL Version"><?php esc_html_e( 'MySQL Version', 'uncode' ); ?>:</td>
				<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'The version of MySQL installed on your hosting server.', 'uncode' ) . '">[?]</a>'; ?></td>
				<td>
					<?php
					/** @global wpdb $wpdb */
					global $wpdb;
					echo esc_html($wpdb->db_version());
					?>
				</td>
			</tr>
			<tr>
				<td data-export-label="Max Upload Size"><?php esc_html_e( 'Max Upload Size', 'uncode' ); ?>:</td>
				<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'The largest filesize that can be uploaded to your WordPress installation.', 'uncode' ) . '">[?]</a>'; ?></td>
				<td><?php
				$php_max_size = (int) size_format( wp_max_upload_size() );
				if ( $php_max_size < 32 ) {
					echo '<mark class="error">' . sprintf( wp_kses(__( '%s - Raccomended value is at least 32. <a href="%s" target="_blank">Please increase it.</a>', 'uncode' ), array( 'a' => array( 'href' => array(),'target' => array() ) ) ), size_format( wp_max_upload_size() ), 'https://www.a2hosting.com/kb/developer-corner/php/using-php-directives-in-custom-htaccess-files/setting-the-php-maximum-upload-file-size-in-an-htaccess-file' ) . '</mark>';
				} else {
					echo '<mark class="yes">' . size_format( wp_max_upload_size() ) . '</mark>';
				}
				?></td>
			</tr>
			<tr>
				<td data-export-label="Allow URL fopen"><?php esc_html_e( 'Allow URL fopen', 'uncode' ); ?>:</td>
				<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'The largest filesize that can be uploaded to your WordPress installation.', 'uncode' ) . '">[?]</a>'; ?></td>
				<td><?php
				if ( !ini_get('allow_url_fopen') ) {
					echo '<mark class="error">' . sprintf( wp_kses(__( 'Disabled - For the import of the demo data this value needs to be enabled. <a href="%s" target="_blank">Please enabled it.</a>', 'uncode' ), array( 'a' => array( 'href' => array(),'target' => array() ) ) ), 'https://www.a2hosting.com/kb/developer-corner/php/using-php.ini-directives/php-allow-url-fopen-directive' ) . '</mark>';
				} else {
					echo '<mark class="yes">' . esc_html__('Enabled','uncode') . '</mark>';
				}
				?></td>
			</tr>
			<tr>
				<td data-export-label="Default Timezone is UTC"><?php esc_html_e( 'Default Timezone is UTC', 'uncode' ); ?>:</td>
				<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'The default timezone for your server.', 'uncode' ) . '">[?]</a>'; ?></td>
				<td><?php
					$default_timezone = date_default_timezone_get();
					if ( 'UTC' !== $default_timezone ) {
						echo '<mark class="error">' . '&#10005; ' . sprintf( esc_html__( 'Default timezone is %s - it should be UTC', 'uncode' ), $default_timezone ) . '</mark>';
					} else {
						echo '<mark class="yes">' . '&#10004;' . '</mark>';
					} ?>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="widefat" cellspacing="0" id="status">
		<thead>
			<tr>
				<th colspan="3" data-export-label="Theme"><?php esc_html_e( 'Theme', 'uncode' ); ?></th>
			</tr>
		</thead>
			<?php
			$active_theme = wp_get_theme();
			?>
		<tbody>
			<tr>
				<td data-export-label="Name" style="width: 33%;"><?php esc_html_e( 'Name', 'uncode' ); ?>:</td>
				<td class="help" style="width: 15px;"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'The name of the current active theme.', 'uncode' ) . '">[?]</a>'; ?></td>
				<td><?php echo esc_html($active_theme->Name); ?></td>
			</tr>
			<tr>
				<td data-export-label="Version"><?php esc_html_e( 'Version', 'uncode' ); ?>:</td>
				<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'The installed version of the current active theme.', 'uncode' ) . '">[?]</a>'; ?></td>
				<td><?php
					echo esc_html($active_theme->Version);

					if ( ! empty( $theme_version_data['version'] ) && version_compare( $theme_version_data['version'], $active_theme->Version, '!=' ) ) {
						echo ' &ndash; <strong style="color:red;">' . $theme_version_data['version'] . ' ' . esc_html__( 'is available', 'uncode' ) . '</strong>';
					}
				?></td>
			</tr>
			<tr>
				<td data-export-label="Author URL"><?php esc_html_e( 'Author URL', 'uncode' ); ?>:</td>
				<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'The theme developers URL.', 'uncode' ) . '">[?]</a>'; ?></td>
				<td><?php echo wp_kses_post($active_theme->{'Author URI'}); ?></td>
			</tr>
			<tr>
				<td data-export-label="Child Theme"><?php esc_html_e( 'Child Theme', 'uncode' ); ?>:</td>
				<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'Displays whether or not the current theme is a child theme.', 'uncode' ) . '">[?]</a>'; ?></td>
				<td><?php
					echo is_child_theme() ? '<mark class="yes">' . '&#10004;' . '</mark>' : '&#10005; &ndash; ' . sprintf( wp_kses(__( 'If you\'re modifying uncode or a parent theme you didn\'t build personally we recommend using a child theme. See: <a href="%s" target="_blank">How to create a child theme</a>', 'uncode' ), array( 'a' => array( 'href' => array(),'target' => array() ) ) ), 'http://codex.wordpress.org/Child_Themes' );
				?></td>
			</tr>
			<?php
			if( is_child_theme() ) :
				$parent_theme = wp_get_theme( $active_theme->Template );
			?>
			<tr>
				<td data-export-label="Parent Theme Name"><?php esc_html_e( 'Parent Theme Name', 'uncode' ); ?>:</td>
				<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'The name of the parent theme.', 'uncode' ) . '">[?]</a>'; ?></td>
				<td><?php echo esc_html($parent_theme->Name); ?></td>
			</tr>
			<tr>
				<td data-export-label="Parent Theme Version"><?php echo esc_html_e( 'Parent Theme Version', 'uncode' ); ?>:</td>
				<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'The installed version of the parent theme.', 'uncode' ) . '">[?]</a>'; ?></td>
				<td><?php echo esc_html($parent_theme->Version); ?></td>
			</tr>
			<tr>
				<td data-export-label="Parent Theme Author URL"><?php esc_html_e( 'Parent Theme Author URL', 'uncode' ); ?>:</td>
				<td class="help"><?php echo '<a href="#" class="help_tip" data-tip="' . esc_attr__( 'The parent theme developers URL.', 'uncode' ) . '">[?]</a>'; ?></td>
				<td><?php echo wp_kses_post($parent_theme->{'Author URI'}); ?></td>
			</tr>
			<?php endif ?>
		</tbody>
	</table>
	<table class="widefat" cellspacing="0" id="status">
		<thead>
			<tr>
				<th colspan="3" data-export-label="Active Plugins (<?php echo count( (array) get_option( 'active_plugins' ) ); ?>)"><?php esc_html_e( 'Active Plugins', 'uncode' ); ?> (<?php echo count( (array) get_option( 'active_plugins' ) ); ?>)</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$active_plugins = (array) get_option( 'active_plugins', array() );

			if ( is_multisite() ) {
				$active_plugins = array_merge( $active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
			}

			foreach ( $active_plugins as $plugin ) {

				$plugin_data    = @get_plugin_data( WP_PLUGIN_DIR . '/' . $plugin );
				$dirname        = dirname( $plugin );
				$version_string = '';
				$network_string = '';

				if ( ! empty( $plugin_data['Name'] ) ) {

					// link the plugin name to the plugin url if available
					$plugin_name = esc_html( $plugin_data['Name'] );

					if ( ! empty( $plugin_data['PluginURI'] ) ) {
						$plugin_name = '<a href="' . esc_url( $plugin_data['PluginURI'] ) . '" title="' . esc_attr__( 'Visit plugin homepage' , 'uncode' ) . '" target="_blank">' . $plugin_name . '</a>';
					}

					?>
					<tr>
						<td style="width: 33%;"><?php echo wp_kses_post($plugin_name); ?></td>
						<td><?php echo sprintf( _x( 'by %s', 'by author', 'uncode' ), $plugin_data['Author'] ) . ' &ndash; ' . esc_html( $plugin_data['Version'] ) . $version_string . $network_string; ?></td>
					</tr>
					<?php
				}
			}
			?>
		</tbody>
	</table>
</div>

<script type="text/javascript">

	jQuery( document ).ready( function ( $ ) {
		$( '.help_tip' ).tipTip({
			attribute: 'data-tip'
		});

		$( 'a.help_tip' ).click( function() {
			return false;
		});

	});

</script>