<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 	WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header( 'shop' );

/**
 * DATA COLLECTION - START
 *
 */

/** Init variables **/
$limit_width = $limit_content_width = $the_content = $main_content = $layout = $sidebar_style = $sidebar_bg_color = $sidebar = $sidebar_size = $sidebar_sticky = $sidebar_padding = $sidebar_inner_padding = $sidebar_content = $title_content = $navigation_content = $page_custom_width = $row_classes = $main_classes = $footer_classes = '';

$post_type = 'product_index';

/** Get general datas **/
$style = ot_get_option('_uncode_general_style');
$bg_color = ot_get_option('_uncode_general_bg_color');
$bg_color = ($bg_color == '') ? ' style-'.$style.'-bg' : ' style-'.$bg_color.'-bg';

/** Get page width info **/
$boxed = ot_get_option('_uncode_boxed');

if ($boxed !== 'on')
{
	$generic_content_full = ot_get_option('_uncode_' . $post_type . '_layout_width');
	if ($generic_content_full === '')
	{
		$main_content_full = ot_get_option('_uncode_body_full');
		if ($main_content_full === '' || $main_content_full === 'off') $limit_content_width = ' limit-width';
	}
	else
	{
		if ($generic_content_full === 'limit')
		{
			$generic_custom_width = ot_get_option('_uncode_' . $post_type . '_layout_width_custom');
			if (is_array($generic_custom_width) && !empty($generic_custom_width))
			{
				$page_custom_width = ' style="max-width: ' . implode("", $generic_custom_width) . ';"';
			}
		}
	}
}

/** Collect header data **/
$page_header_type = ot_get_option('_uncode_' . $post_type . '_header');
if ($page_header_type !== '' && $page_header_type !== 'none')
{
	$metabox_data['_uncode_header_type'] = array(
		$page_header_type
	);
	$meta_data = uncode_get_general_header_data($metabox_data, $post_type, '');
	$metabox_data = $meta_data['meta'];
	$show_title = $meta_data['show_title'];
}

/** Get layout info **/
$activate_sidebar = ot_get_option('_uncode_' . $post_type . '_activate_sidebar');
if ($activate_sidebar !== 'off')
{
	$layout = ot_get_option('_uncode_' . $post_type . '_sidebar_position');
	if ($layout === '') $layout = 'sidebar_right';
	$sidebar = ot_get_option('_uncode_' . $post_type . '_sidebar');
	$sidebar_style = ot_get_option('_uncode_' . $post_type . '_sidebar_style');
	$sidebar_size = ot_get_option('_uncode_' . $post_type . '_sidebar_size');
	$sidebar_sticky = ot_get_option('_uncode_' . $post_type . '_sidebar_sticky');
	$sidebar_sticky = ($sidebar_sticky === 'on') ? ' sticky-sidebar' : '';
	$sidebar_fill = ot_get_option('_uncode_' . $post_type . '_sidebar_fill');
	$sidebar_bg_color = ot_get_option('_uncode_' . $post_type . '_sidebar_bgcolor');
	$sidebar_bg_color = ($sidebar_bg_color !== '') ? ' style-' . $sidebar_bg_color . '-bg' : '';
	if ($sidebar_style === '') $sidebar_style = $style;
}

/** Get title info **/
$generic_show_title = ot_get_option('_uncode_' . $post_type . '_title');
$show_title = ($generic_show_title === 'off') ? false : true;

$get_title = woocommerce_page_title(false);

/**
 * DATA COLLECTION - END
 *
 */

/** Build header **/
if ($page_header_type !== '' && $page_header_type !== 'none')
{
	$page_header = new unheader($metabox_data, $get_title);

	$header_html = $page_header->html;
	if ($header_html !== '') {
		echo '<div id="page-header">';
		echo do_shortcode( shortcode_unautop( $page_header->html ) );
		echo '</div>';
	}
}
echo '<script type="text/javascript">UNCODE.initHeader();</script>';
/** Build title **/

if ($show_title)
{
	$title_content = '<div class="post-title-wrapper"><h1 class="post-title">' . $get_title . '</h1></div>';
}

	$archive_descritption = do_action( 'woocommerce_archive_description' );

	ob_start();
	woocommerce_result_count();
	$counter_content = ob_get_clean();

	ob_start();
	woocommerce_catalog_ordering();
	$ordering_content = ob_get_clean();

	$body_head =
		'<div class="row-inner">
			<div class="col-lg-6">
				<div class="uncol">
					<div class="uncoltable">
						<div class="uncell no-block-padding">
							<div class="uncont">
								'.$title_content.$archive_descritption.$counter_content.'
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="uncol">
					<div class="uncoltable">
						<div class="uncell no-block-padding">
							<div class="uncont">
								'.$ordering_content.'
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>';

		$the_content .= uncode_get_row_template($body_head, $limit_width, $limit_content_width, $style, '', false, false, true);

if (have_posts()):

	woocommerce_product_subcategories();
	global $wp_query;
	$posts_counter = $wp_query->post_count;

	$the_content .=
		'<div id="index-' . big_rand() . '" class="isotope-system' . (($posts_counter === 1) ? ' index-single' : '') . '">
			<div class="isotope-wrapper single-gutter">
				<div class="isotope-container isotope-layout style-masonry isotope-pagination" data-type="masonry" data-layout="fitRows" data-lg="800">';

	/* Start the Loop */
	while ( have_posts() ) : the_post();

		ob_start();
		wc_get_template_part( 'content', 'product' );
		$the_content .= ob_get_clean();

	endwhile;

	$the_content .=
				'</div>
			</div>
		</div>';

	else :

		ob_start();
		get_template_part('content', 'none');
		$the_content .= ob_get_clean();

	endif;

	if ($layout === 'sidebar_right' || $layout === 'sidebar_left')
	{

		/** Build structure with sidebar **/

		if ($sidebar_size === '') $sidebar_size = 4;
		$main_size = 12 - $sidebar_size;
		$expand_col = '';

		/** Collect paddings data **/

		$footer_classes = ' no-top-padding double-bottom-padding';

		if ($sidebar_bg_color !== '')
		{
			if ($sidebar_fill === 'on')
			{
				$sidebar_inner_padding.= ' std-block-padding';
				$sidebar_padding.= $sidebar_bg_color;
				$expand_col = ' unexpand';
				if ($limit_content_width === '')
				{
					$row_classes.= ' no-h-padding col-no-gutter no-top-padding';
					$footer_classes = ' std-block-padding no-top-padding';
					$main_classes.= ' std-block-padding';
				}
				else
				{
					$row_classes.= ' no-top-padding';
					$main_classes.= ' double-top-padding';
				}
			}
			else
			{
				$row_classes.= ' double-top-padding';
				$sidebar_inner_padding.= $sidebar_bg_color . ' single-block-padding';
			}
		}
		else
		{
			$row_classes.= ' col-std-gutter double-top-padding';
			$main_classes.= ' double-bottom-padding';
		}

		$row_classes.= ' no-bottom-padding';
		$sidebar_inner_padding.= ' double-bottom-padding';

		/** Build sidebar **/

		$sidebar_content = "";
		ob_start();
		if ($sidebar !== '')
		{
			dynamic_sidebar($sidebar);
		}
		else
		{
			dynamic_sidebar();
		}
		$sidebar_content = ob_get_clean();

		/** Create html with sidebar **/

		$the_content = '<div class="post-content style-' . $style . $main_classes . '">' . $the_content . '</div>';

		$main_content = '<div class="col-lg-' . $main_size . '">
											' . $the_content . '
										</div>';

		$the_content = '<div class="row-container">
        							<div class="row row-parent' . $row_classes . $limit_content_width . '"' . $page_custom_width . '>
												<div class="row-inner">
													' . (($layout === 'sidebar_right') ? $main_content : '') . '
													<div class="col-lg-' . $sidebar_size . '">
														<div class="uncol style-' . $sidebar_style . $expand_col . $sidebar_padding . $sidebar_sticky . '">
															<div class="uncoltable">
																<div class="uncell' . $sidebar_inner_padding . '">
																	<div class="uncont">
																		' . $sidebar_content . '
																	</div>
																</div>
															</div>
														</div>
													</div>
													' . (($layout === 'sidebar_left') ? $main_content : '') . '
												</div>
											</div>
										</div>';
	} else {

		/** Create html without sidebar **/
		$the_content = '<div class="post-content"' . $page_custom_width . '>' . uncode_get_row_template($the_content, $limit_width, $limit_content_width, $style, '', 'double', true, 'double') . '</div>';

	}

	/** Build and display navigation html **/
	$navigation_option = ot_get_option('_uncode_' . $post_type . '_navigation_activate');
	if ($navigation_option !== 'off')
	{
		$navigation = uncode_posts_navigation();
		if (!empty($navigation) && $navigation !== '') $navigation_content = uncode_get_row_template($navigation, '', $limit_content_width, $style, ' row-navigation row-navigation-' . $style, true, true, true);
	}

	/** Display post html **/
	echo '<div class="page-body' . $bg_color . '">
          <div class="post-wrapper">
          	<div class="post-body">' . do_shortcode($the_content) . '</div>' .
          	$navigation_content . '
          </div>
        </div>';

// end of the loop.

get_footer( 'shop' ); ?>